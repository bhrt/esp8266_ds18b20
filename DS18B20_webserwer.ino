#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#include <ESP8266mDNS.h>

//file with credentials to your wifi. File added to .gitignore
#include "credentials.h"

#define ONE_WIRE_BUS 2
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);


#if !defined(WLAN_SSID)
#define WLAN_SSID ""
#endif
#if !defined(WLAN_PASS)
#define WLAN_PASS ""
#endif
ESP8266WebServer server(80);
String webString="";


void setup(void)
{
    Serial.begin(115200);
    sensors.begin();

    WiFi.begin(WLAN_SSID, WLAN_PASS);
    Serial.print("\n\r \n\rWorking to connect");

    while (WiFi.status() != WL_CONNECTED) 
    {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("DS18B20 Temperature Reading Server");
    Serial.print("Connected to ");
    Serial.println(WLAN_SSID);

    if (!MDNS.begin("esp8266")) 
    {
        Serial.println("Error setting up MDNS responder!");
        while(1) 
        { 
            delay(1000);
        }
    }
    Serial.println("mDNS responder started");

    server.on("/", handle_root);

    server.on("/temp", []()
    {
        String webString = readAllSensors();
        server.send(200, "text/plain", webString);
        delay(100);
    });


    server.on("/config", []()
    {
        webString = "TODO:Config Page";
        server.send(200, "application/json;charset=UTF-8", webString);
        delay(100);
    });

    server.begin();
    Serial.println("HTTP server started: [http://esp8266.local/]");
    MDNS.addService("http", "tcp", 80);
}

void loop(void)
{
    server.handleClient();
} 

String readSensor(const uint8_t i) 
{
    sensors.requestTemperaturesByIndex(i);
    delay(100);
    return String(sensors.getTempCByIndex(i));
}


String readAllSensors() 
{
    String output = "";
    for(uint8_t i=0;i<sensors.getDeviceCount();i++)
    {
        output += "Temperature of sensor "+String(i)+" [adress: "+ printAddress(i) +" ] is "+readSensor(i) +" C\n\r";
    }
    return output;
}

String printAddress(const uint8_t i)
{
    DeviceAddress deviceAddress;
    sensors.getAddress(deviceAddress,i);
    String output = "";
    for (uint8_t i = 0; i < 8; i++)
    {
        if (deviceAddress[i] < 16)
        {
            output+="0";
        }
        output+=String(deviceAddress[i], HEX);
    }
    return output;
}

void handle_root() 
{
    server.send(200, "text/plain", "Hello from the weather esp8266, read from /temp or /config");
    delay(100);
}